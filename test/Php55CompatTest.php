<?php

class Php55CompatTest extends PHPUnit_Framework_TestCase
{

    public function testArrayColumn ()
    {
        $recordset = array(
            array('col1' => 'one',   'col2' => 1),
            array('col1' => 'two',   'col2' => null),
            array('col1' => 'three', 'col2' => 3, 'col3' => 'III'),
            array('col1' => 'four',  'col2' => ''),
            array('col1' => 'five',  'col2' => 5, 'col3' => 'V')
        );
        $this->assertEquals(array(1, null, 3, '', 5), php55_array_column($recordset, 'col2'));
        $this->assertEquals(array('one', 'two', 'three', 'four', 'five'), php55_array_column($recordset, 'col1'));
        $this->assertEquals(array('one' => 1, 'two' => null, 'three' => 3, 'four' => '', 'five' => 5), php55_array_column($recordset, 'col2', 'col1'));
        $this->assertEquals(array(1 => 'one', 3 => 'three', '' => 'four', 5 => 'five'), php55_array_column($recordset, 'col1', 'col2'));
        $this->assertEquals(array(1, null, 'III' => 3, '', 'V' => 5), php55_array_column($recordset, 'col2', 'col3'));
        $this->assertEquals(array('one' => null, 'two' => null, 'three' => 'III', 'four' => null, 'five' => 'V'), php55_array_column($recordset, 'col3', 'col1'));
    }

}