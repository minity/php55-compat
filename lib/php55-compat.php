<?php
if (!function_exists('array_column')) {
    /**
     * Get the values from a single column in the input array
     * Returns the values from a single column of the array, identified by the $column_key.
     * Optionally, you may provide an $index_key to index the values in the returned array
     * by the values from the $index_key column in the input array.
     * @param array $array
     * @param string $column_key
     * @param string $index_key
     * @return array
     */
    function array_column(array $array, $column_key, $index_key = null)
    {
        return call_user_func_array('php55_array_column', func_get_args());
    }
}

function php55_array_column(array $array, $column_key, $index_key = null)
{
    $result = array();
    foreach ($array as $row) {
        if (null !== $index_key && array_key_exists($index_key, $row)) {
            $result[$row[$index_key]] = @$row[$column_key];
        } else {
            $result[] = @$row[$column_key];
        }
    }
    return $result;
}
